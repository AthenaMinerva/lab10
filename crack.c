#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary) {
    // Open the dictionary file
    FILE *fp;
    fp = fopen(dictionary, "r");

    // Loop through the dictionary file, one line at a time.
    static char password[PASS_LEN];
    while(fgets(password, PASS_LEN, fp) != NULL) {
        // replace newline char
        char *nl = strchr(password, '\n');
        if (nl != NULL) *nl = '\0';

        char *hashed = md5(password, strlen(password));

        if(!strcmp(hashed, target)) {
            free(hashed);
            fclose(fp);
            return password;
        }

        free(hashed);
    }
    
    // Close file to Free up memory
    fclose(fp);

    return "Password Not Found";
}


int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *fp;
    fp = fopen(argv[1], "r");

    // For each hash, crack it by passing it to crackHash
    char hash[HASH_LEN+1];
    while(fgets(hash, HASH_LEN+1, fp) != NULL) {
        // replace newline char
        char *nl = strchr(hash, '\n');
        if (nl != NULL) *nl = '\0';

        char * password = crackHash(hash, argv[2]);

        // Display the hash along with the cracked password:
        // d41402abc4b2a76b9719d911017c592 hello
        printf("%s %s\n", hash, password);
    }
    
    // Close the hash file
    fclose(fp);
}
